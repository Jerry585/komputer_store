// Runs the loadData function when the page gets reloaded.
if (window.performance.getEntriesByType("navigation").map((navigation) => navigation.type).includes("reload")) {
    loadData()
}

// Load the balance and loan data from the session storage.
function loadData() {
    document.getElementById("pay").innerHTML = sessionStorage.getItem("currentPayBalance")
}

// Earn money.
function Work() {
    let pay = parseInt(document.getElementById("pay").innerHTML)

    pay += 100
    document.getElementById("pay").innerHTML = pay
    sessionStorage.setItem("currentPayBalance", pay)
}

// Send the money to the bank.
function Bank() {
    let newBalance = parseInt(sessionStorage.getItem("currentBalance"))
    let newLoan = parseInt(sessionStorage.getItem("currentLoan"))
    
    if (sessionStorage.getItem("currentLoan") > 0) {
        let currentPay = parseInt(sessionStorage.getItem("currentPayBalance"))
        currentPay *= 0.9
        newBalance += currentPay

        let tmpLoan = parseInt(sessionStorage.getItem("currentPayBalance"))
        tmpLoan *= 0.1
        
        // Fixes negative loan.
        newLoan -= tmpLoan
        if (newLoan < 0) {
            let tmpRest = -newLoan
            newLoan = tmpRest
            newBalance += newLoan
            
            document.getElementById("loan").innerHTML = 0
            sessionStorage.setItem("currentLoan", 0)
        }
        else {
            document.getElementById("loan").innerHTML = newLoan            
            sessionStorage.setItem("currentLoan", newLoan)
        }
    }
    else {
        newBalance += parseInt(sessionStorage.getItem("currentPayBalance"))
    }

    if (parseInt(sessionStorage.getItem("currentLoan")) > 0) {
        document.getElementById("repay").style.display = "block"
    }
    else {
        document.getElementById("repay").style.display = "none"      
    }
    
    document.getElementById("balance").innerHTML = newBalance

    sessionStorage.setItem("currentBalance", newBalance)
    sessionStorage.setItem("currentPayBalance", "0")
    document.getElementById("pay").innerHTML = 0
}

// Repay the whole loan or the total value of the pay balance.
function Repay() {
    let loan = parseInt(sessionStorage.getItem("currentLoan"))
    let pay = parseInt(sessionStorage.getItem("currentPayBalance"))

    if (loan >= pay) {
        loan -= pay
        sessionStorage.setItem("currentLoan", loan)
    }
    else {
        let tmpPay = pay
        tmpPay -= loan
        let balance = parseInt(sessionStorage.getItem("currentBalance"))
        balance += tmpPay
        sessionStorage.setItem("currentBalance", balance)
        document.getElementById("balance").innerHTML = balance
        sessionStorage.setItem("currentLoan", 0)
        document.getElementById("loan").innerHTML = "0"
    }
}