// Fixes so that the values are correct when the page
// gets loaded for the first time. Applies for both
// if statements.

if (!sessionStorage.getItem("currentBalance")) {
    sessionStorage.setItem("currentBalance", 0)
    document.getElementById("balance").innerHTML = sessionStorage.getItem("currentBalance")
}

if (!sessionStorage.getItem("currentPayBalance")) {
    sessionStorage.setItem("currentPayBalance", 0)
    document.getElementById("pay").innerHTML = sessionStorage.getItem("currentPayBalance")
}