// Runs the loadData function when the page gets reloaded.
if (window.performance.getEntriesByType("navigation").map((navigation) => navigation.type).includes("reload")) {
    loadData()
}

// Load the balance and loan data from the session storage.
function loadData() {
    document.getElementById("balance").innerHTML = sessionStorage.getItem("currentBalance")
    document.getElementById("loan").innerHTML = sessionStorage.getItem("currentLoan")
}

// Function for getting a loan
function getLoan() {
    let loan = parseInt(prompt("How big a loan do you want?"))

    if (loan > parseInt(sessionStorage.getItem("currentBalance")) * 2) {
        alert(`The loan is too big. Your max loan is ${parseInt(sessionStorage.getItem("currentBalance")) * 2}Kr.`)
    }
    else if (sessionStorage.getItem("currentLoan") > 0) {
        alert("You already have a loan.")
    }
    else if (isNaN(loan)) {
        alert("Oops, something went wrong. Please try again later.")
    }
    else {
        document.getElementById("loan").innerHTML = loan
        sessionStorage.setItem("currentLoan", loan)

        let newBalance = parseInt(sessionStorage.getItem("currentBalance"))
        newBalance += loan
        sessionStorage.setItem("currentBalance", newBalance)
        document.getElementById("balance").innerHTML = newBalance
    }

    // Displays or hides the repay button.
    if (loan > 0) {
        document.getElementById("repay").style.display = "block"
    }
    else {
        document.getElementById("repay").style.display = "none"       
    }
}