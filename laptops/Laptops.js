let laptops = null

// Fetch the laptops data.
async function getLaptops() {
    await fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then((res) => {
        if (res.ok) {
            return res.json()
        }
        else {
            throw new Error(res.Error)
        }
    })
    .then((data) => {
        showLaptops(data)
        laptops = data
    })
    .catch((error) => console.error(error))
}

// Displays all laptops in an select box.
function showLaptops(laptopsData) {
    for (const data of laptopsData) {
        let element = document.createElement("option")
        element.setAttribute("value", data.id)
        element.setAttribute("onchange", "showFeatures()")
        let title = document.createTextNode(data.title)
        element.appendChild(title)

        let parentNode = document.getElementById("dropdownMenu")
        parentNode.appendChild(element)
    }
}

// Shows all the features for the specified laptop.
function showFeatures() {
    let selectedLaptop = document.getElementById("dropdownMenu").value
    let specs = laptops[selectedLaptop - 1].specs
    let specText = ""
    for (const spec of specs) {
        specText += spec
        specText += "\n"
    }

    document.getElementById("features").innerHTML = specText
    showLaptopDetails()
}

// Displays the laptop details such as price, description and image.
function showLaptopDetails() {
    let selectedLaptop = document.getElementById("dropdownMenu").value

    let image = "https://noroff-komputer-store-api.herokuapp.com/"
    image += laptops[selectedLaptop - 1].image
    let description = laptops[selectedLaptop - 1].description
    let title = laptops[selectedLaptop - 1].title
    let price = laptops[selectedLaptop - 1].price
    price += " Kr"

    document.getElementById("cardTitle").innerHTML = title
    document.getElementById("cardDescription").innerHTML = description
    document.getElementById("cardPrice").innerHTML = price

    let img = document.querySelector("img")
    img.src = image
}

// Function to be able to buy the laptop.
function buyLaptop() {
    let selectedLaptop = document.getElementById("dropdownMenu").value

    if (sessionStorage.getItem("currentBalance") >= laptops[selectedLaptop - 1].price) {
        let newBalance = sessionStorage.getItem("currentBalance")
        newBalance -= laptops[selectedLaptop - 1].price
        sessionStorage.setItem("currentBalance", newBalance)
        document.getElementById("balance").innerHTML = newBalance

        alert("You got a new laptop")
    }
    else{
        alert("Not enough money")
    }
}

getLaptops()